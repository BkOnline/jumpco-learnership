<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Graduate Job Portal - About Us</title>
<style type="text/css">
  <%@include file="style/style.css" %>
  </style>
  <script type="text/javascript">
  <%@include file="js/script.js" %>
  </script>
</head>

<body>
	<div class="topnav" id="myTopnav">
  <a href="index.html" class="active">Home</a>
  <a href="login.jsp">Login</a>
  <a href="aboutUs.jsp">About Us</a>
  <a href="SignUpAdRE.jsp">Recruiter Sign Up</a>
  <a href="SignUpAsJS.jsp">Job Seeker Sign Up</a>
  <a href="javascript:void(0);" class="icon" onclick="myFunction()">
    <i class="fa fa-bars"></i>
  </a>
</div>
<div class="container">
    <p>
    	The Online graduate portal is aimed at developing or building an online recruit portal for graduates fresh out of school seeking to kick start their careers. 
    	The system will be an online web application which can be accessed anywhere only with proper login credentials provided. 
    	The system will be used as an Online Portal for Graduates seeking opportunities to further their careers. 
    	Graduates should be able to login and upload all relevant transcripts. Companies, recruiter and organizations may also register, login and access or search any information uploaded by the Graduates.
    </p>
  </div>
</body>
</html>